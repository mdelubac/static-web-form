function verification(){
	var nom = document.forms["MyForm"]["nom"].value;
	if(nom==""){
		alert("Merci de renseigner votre nom");
		return false;
	}

	var prenom = document.forms["MyForm"]["prenom"].value;
	if(prenom==""){
		alert("Merci de renseigner votre prenom");
		return false;
	}

	var patt = /^([a-zA-Z]+)@([a-zA-Z]{4,6})\.([a-zA-Z]{2,3})$/ 
	var mail = document.forms["MyForm"]["mail"].value;
	if(mail==""){
		alert("Merci de renseigner votre mail");
		return false;
	}
	else if (patt.test(mail)===false){
		alert("Erreur de saisie de votre e-mail");
	}


	var message = document.forms["MyForm"]["message"].value;
	if(message==""){
		alert("Merci d'ecire votre message");
		return false;
	}

	return true;


}